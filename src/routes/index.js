import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

export default (app, db) => {
    app.post('/register', async (req, res) => {
        const {login, password} = req.body
        try {
            const isCheckedLogin = await collection.findOne({login});
            if (!!isCheckedLogin) {
                res.send({
                    msg:'Простите но кажется мы уже знакомы не хотите авторизоватся?',
                    success:false,
                    token:undefined
                })
            } else {
                const newPassword = await bcrypt.hash(password, 10);
                collection.insertOne({password: newPassword, login}, (err, docsInserted) => {
                    const id = docsInserted.insertedId
                    res.send({
                        msg:'Успешная регистрация',
                        success:true,
                        token:jwt.sign({id}, secret)
                    })
                })
            }
        } catch (e) {
            res.status(401).send({
                success: false,
                msg: 'Оу кажется мы с тобой не знакомы, точно всё правильно ввёл?'
            })
        }

    })

    app.get('/userme', async (req,res)=>{
        let {authorization} = req.headers;
        authorization = authorization.slice(7);
        console.log(authorization);

        const decoded = jwt.verify(authorization, secret);
        console.log(decoded.id);
        const user = await collection.findOne({_id:ObjectId(decoded.id)});
        console.log(user);
        if(!!user){
            res.send({
                success:true,
                user,
                msg:'Успешная авторизация'
            })
        }else{
            res.send({
                success:false,
                msg:'Пользователь ещё не авторизован'
            })
        }
    })

    app.post('/login', async  (req, res) => {
        const {login,password} = req.body;
        try{
            const isCheckedUser = await collection.findOne({login});
            const isCheсkedPassword = await bcrypt.compare(password, isCheckedUser.password);
            const id = isCheckedUser._id;
            if(!isCheckedUser){
                res.send({
                    msg:'Пользователь с таким именем не найден',
                    success:false
                })
            }else if(isCheсkedPassword){
                res.send({
                    msg:'Успешная авторизация',
                    success:true,
                    token:jwt.sign({id},secret)
                })
            }else{
                res.send({
                    msg:'Оу а такого пользователя у нас нет',
                    success:false,
                })

            }
        }catch (e) {
            res.send({
                success: false,
                msg: 'Оу что-то пошло не так повтори попытку позже'
            })
        }
    })
})

}