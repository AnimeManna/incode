const express = require('express');
const bodyParser = require('body-parser');
const mongoClient = require("mongodb").MongoClient;
const ObjectId = require('mongodb').ObjectId
const cors = require('cors');
const server = express();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')


const url = "mongodb://localhost:27017/";

server.use(bodyParser.json())
server.use(cors());
server.use(bodyParser.urlencoded({
    extended: true

}))

const secret = 'cAtwa1kkEysags1t25ag6yk754'

mongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    const db = client.db("users");

    server.post('/register', async (req, res) => {
        let collection = db.collection("users");
        const {login, password} = req.body;
        try {
            const loginVerificationResult = await collection.findOne({login});
            if (!!loginVerificationResult) {
                res.send({
                    msg: 'Простите но кажется мы уже знакомы не хотите авторизоватся?',
                    success: false,
                    token: undefined
                })
            } else {
                const newPassword = await bcrypt.hash(password, 10);
                collection.insertOne({password: newPassword, login}, (err, docsInserted) => {
                    const id = docsInserted.insertedId
                    res.send({
                        msg: 'Успешная регистрация',
                        success: true,
                        token: jwt.sign({id}, secret)
                    })
                })
            }
        } catch (e) {
            res.status(401).send({
                success: false,
                msg: 'Оу кажется что-то пошло не так, повторите попытку позже'
            })
        }

    })

    server.get('/userme', async (req, res) => {
        let collection = db.collection("users");
        console.time('user-req');
        let jsonwt = jwt;
        let {authorization} = req.headers;
        authorization = authorization.slice(7);
        console.log('HEADER: ', authorization);
        let decoded;
        try {
            decoded = jsonwt.verify(authorization, secret);
            console.log('DECODED: ', decoded);
        } catch (error) {
            return res.status(401).json({
                success: false,
                error: 'no token provided'
            })
        }
        const user = await collection.findOne({_id: ObjectId(decoded.id)});
        if (!!user) {
            console.log('GET USER TOOK:')
            console.timeEnd('user-req');
            console.log('\n\n\n\n');
            res.send({
                success: true,
                user,
                msg: 'Успешная авторизация'
            })
        } else {
            console.log('GET USER WITH ERROR TOOK:', user);
            console.timeEnd('user-req');
            console.log('\n\n\n\n');
            res.send({
                success: false,
                msg: 'Пользователь ещё не авторизован'
            })
        }
    })


    server.post('/login', async (req, res) => {
        let collection = db.collection("users");
        const {login, password} = req.body;
        try {
            const isCheckedUser = await collection.findOne({login});
            const isCheckedPassword = await bcrypt.compare(password, isCheckedUser.password);
            const id = isCheckedUser._id;
            if (!isCheckedUser) {
                res.send({
                    msg: 'Пользователь с таким именем не найден',
                    success: false
                })
            } else if (isCheckedPassword) {
                res.send({
                    msg: 'Успешная авторизация',
                    success: true,
                    token: jwt.sign({id}, secret)
                })
            } else {
                res.send({
                    msg: 'Оу а такого пользователя у нас нет',
                    success: false,
                })

            }
        } catch (e) {
            res.send({
                success: false,
                msg: 'Оу кажется что-то пошло не так, повторите попытку позже'
            })
        }
    })

    server.get('/getPosts', async (req, res) => {
        let collection = db.collection("posts");
        try {
            let posts = await collection.find().toArray();
            res.send({
                success: true,
                msg: 'Успешно получены',
                posts: posts
            })
        } catch (e) {
            res.status(404).send({
                msg: 'Оу, что-то пошло не так',
                success: false,
                posts: undefined
            })
        }

    })

    server.post('/newPost', async (req, res) => {
        const {title, content, author} = req.body
        let collection = db.collection("posts");
        try {
            if (!!title && !!content && !!author) {
                const response = await collection.insertOne({title, content, author});
                console.log(response.ops);
                if (!!response) {
                    res.send({
                        msg: "Отлично, ваш пост добавлен",
                        success: true
                    })
                }
            } else {
                res.send({
                    "msg": 'Кажется вы что-то забыли заполнить',
                    success: false
                })
            }
        } catch (e) {
            res.status(500).send({
                msg: 'Упс, кажется не получилось добавить ваш пост',
                success: false
            })
        }

    })

    server.post('/changePostUpdate', async (req,res)=>{
        const {_id} = req.body

        let collection = db.collection("posts");
        try{
            const result = await collection.findOne({_id:ObjectId(_id)})
            if(!!result){
                res.send({
                    msg:'Успешно найден',
                    success:true,
                    post:result
                })
            }else{
                res.send({
                    msg:'Оу ничего не найдено',
                    success:false
                })
            }
        }catch (e) {
            res.send({
                msg:"Упс что-то пошло не так"
            })
        }

    })

    server.post('/updatePost', async (req, res) => {
        const {_id, titleUpdate, contentUpdate} = req.body.info
        let collection = db.collection("posts");
        try {
            const result = await collection.updateOne({_id: ObjectId(_id)}, {
                $set: {
                    title: titleUpdate,
                    content: contentUpdate
                }
            });
            if (!!result) {
                res.send({
                    msg: "Запись обновлена",
                    success: true
                })
            }else{
                res.send({
                    msg:'Что-то пошло не так',
                    success:false
                })
            }
        } catch (e) {
            res.status(404).send({
                msg: 'Упс не получилось обновить страницу',
                success: false
            })
        }
    })

    server.post('/deletePost', async (req, res) => {
        const {_id} = req.body
        let collection = db.collection("posts");
        try {
            let result = await collection.deleteOne({_id: ObjectId(_id)});
            console.log(result)
            res.send({
                msg: 'Ваша запись удалена',
                success: true
            })
        } catch (e) {
            res.status(405).send({
                msg: 'Оопс что-то пошло не так повторите позже',
                success: false
            })
        }
    })

});

server.listen(8000, () => {
    console.log('Вы подключились к localhost:8000')
});
